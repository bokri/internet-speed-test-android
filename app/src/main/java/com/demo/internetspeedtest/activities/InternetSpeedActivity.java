package com.demo.internetspeedtest.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Default abstract activity for all default values
 * */
public abstract class InternetSpeedActivity
    extends AppCompatActivity
{

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
  }
}
