package com.demo.internetspeedtest.activities;

import android.os.Bundle;

import com.demo.internetspeedtest.R;
import com.demo.internetspeedtest.receivers.SpeedTestReceiver;

/**
 * Our first and Main activity
 * */
public class MainActivity extends InternetSpeedActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SpeedTestReceiver.setupAlarm(getApplicationContext());
    }
}
