package com.demo.internetspeedtest.activities;

import android.os.Bundle;

import com.demo.internetspeedtest.R;

/**
 * This activity is reachable only if the user clicks on the notification
 * */
public class NotificationActivity
    extends InternetSpeedActivity
{

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_notification);
  }
}
