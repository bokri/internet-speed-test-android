package com.demo.internetspeedtest.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * This is a starter receiver to setup our alarm after reboot
 * */
public final class SpeedTestStarterReceiver extends BroadcastReceiver {

  @Override
  public void onReceive(Context context, Intent intent) {
    SpeedTestReceiver.setupAlarm(context);
  }
}
