package com.demo.internetspeedtest.receivers;

import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.demo.internetspeedtest.constants.Constants;
import com.demo.internetspeedtest.services.SpeedTestIntentService;

/**
 * This is the speed test receiver that fires the alarm and receives the alarm actions
 * */
public class SpeedTestReceiver
    extends BroadcastReceiver
{

  private static final String ACTION_START_SPEED_TEST_SERVICE = "ACTION_START_SPEED_TEST_SERVICE";

  public static void setupAlarm(Context context) {
    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    PendingIntent alarmIntent = getStartPendingIntent(context);
    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
        getTriggerAt(new Date()),
        Constants.DELAY,
        alarmIntent);
  }

  private static long getTriggerAt(Date now) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(now);
    return calendar.getTimeInMillis();
  }

  private static PendingIntent getStartPendingIntent(Context context) {
    Intent intent = new Intent(context, SpeedTestReceiver.class);
    intent.setAction(ACTION_START_SPEED_TEST_SERVICE);
    return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }


  @Override
  public void onReceive(Context context, Intent intent)
  {String action = intent.getAction();
    Intent serviceIntent = null;
    if (ACTION_START_SPEED_TEST_SERVICE.equals(action)) {
      Log.i(getClass().getSimpleName(), "onReceive from alarm, starting speed test service");
      serviceIntent = SpeedTestIntentService.createIntentStartSpeedTeserService(context);
    }

    if (serviceIntent != null) {
      context.startService(serviceIntent);
    }
  }
}
