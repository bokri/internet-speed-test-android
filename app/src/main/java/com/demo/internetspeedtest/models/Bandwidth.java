package com.demo.internetspeedtest.models;

import io.realm.RealmObject;

/**
 * POJO Object to present the bandwidth
 * */
public class Bandwidth extends RealmObject
{

  public Double speed;
  public Long time;

  public Bandwidth(Double speed, Long time) {

    this.speed = speed;
    this.time = time;
  }

  public Bandwidth() {

    this.speed = 0.0;
    this.time = (long) 0;
  }


  @Override
  public String toString()
  {
    return "Bandwidth{" +
        "speed=" + speed +
        ", time=" + time +
        '}';
  }
}
