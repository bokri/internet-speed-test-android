package com.demo.internetspeedtest.network;

/**
 * Created by aymenbokri on 17/08/2017.
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;
import android.util.Log;

import com.demo.internetspeedtest.constants.Constants;
import com.demo.internetspeedtest.models.Bandwidth;

/**
 * AsyncTask for handling calculation of bandwidth
 */
public class SpeedTesterTask extends AsyncTask<Void, Void, Bandwidth>
{

  @Override
  public Bandwidth doInBackground(Void... params) {
    try {
      long startTime = System.currentTimeMillis();
      // Open a stream to download the image from our URL.
      URLConnection connection = new URL(Constants.SPEED_TEST_BASE_URL_1_MO).openConnection();
      connection.setUseCaches(false);
      connection.connect();
      InputStream input = connection.getInputStream();
      try {
        byte[] buffer = new byte[1024];

        // Do some busy waiting while the stream is open.
        while (input.read(buffer) != -1) {
        }
        long endTime = System.currentTimeMillis();

        Double durationInSeconds = (endTime - startTime) / 1000.0;

        double speed = (1 / durationInSeconds);

        return new Bandwidth(speed, endTime);

      } finally {
        input.close();
      }
    } catch (IOException e) {
      Log.e(getClass().getName(), "Error while downloading file.");
      return new Bandwidth();
    }
  }
}