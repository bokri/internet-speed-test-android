package com.demo.internetspeedtest.data;

import android.content.Context;
import android.util.Log;

import com.demo.internetspeedtest.models.Bandwidth;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A helper class to manage data saved into Realm Database
 */

public class RealmHelper
{
  public RealmHelper(Context context) {

    Realm.init(context);
    realm = Realm.getDefaultInstance();
  }

  private final Realm realm;

  public void saveBandwidth(Double speed, Long time) {

    realm.beginTransaction();
    Bandwidth bandwidth = realm.createObject(Bandwidth.class);
    bandwidth.speed = speed;
    bandwidth.time = time;
    realm.commitTransaction();

    Log.d(getClass().getName(), "Bandwidth saved : "+speed);
  }

  public boolean isDecreasing(Double speed, Long time) {

    Bandwidth bandwidth = realm.where(Bandwidth.class).findAllSorted("time", Sort.DESCENDING).first(new Bandwidth());
    Log.d(getClass().getName(), "Actual speed : "+speed);
    Log.d(getClass().getName(), "Last speed : "+bandwidth.toString());
    if(speed<bandwidth.speed)
      return true;
    return false;

  }
}
