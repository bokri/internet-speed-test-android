package com.demo.internetspeedtest.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.demo.internetspeedtest.R;
import com.demo.internetspeedtest.activities.NotificationActivity;
import com.demo.internetspeedtest.data.RealmHelper;
import com.demo.internetspeedtest.models.Bandwidth;
import com.demo.internetspeedtest.network.SpeedTesterTask;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */


public class SpeedTestIntentService extends IntentService {

  private static final int NOTIFICATION_ID = 1;
  private static final String ACTION_START = "ACTION_START";


  public SpeedTestIntentService() {

    super(SpeedTestIntentService.class.getSimpleName());

  }

  public static Intent createIntentStartSpeedTeserService(Context context) {
    Intent intent = new Intent(context, SpeedTestIntentService.class);
    intent.setAction(ACTION_START);
    return intent;
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Log.d(getClass().getSimpleName(), "onHandleIntent, started handling a speed test event");
    try {
      String action = intent.getAction();
      if (ACTION_START.equals(action)) {
        processStartSpeedTester();
      }
    } catch(Exception exception) {
      exception.printStackTrace();
    }
  }


  private void processStartSpeedTester() {

    try
    {
      // Fire an async task to calculate speed
      SpeedTesterTask task = new SpeedTesterTask();
      task.execute();

      // Get the result from the thread
      Bandwidth bandwidth = task.get();

      RealmHelper helper = new RealmHelper(getApplicationContext());

      // If the internet speed is decreasing sho a notification
      if(helper.isDecreasing(bandwidth.speed, bandwidth.time))
      {
        createNotification(bandwidth);
      }
      // Save the new internet speed
      helper.saveBandwidth(bandwidth.speed, bandwidth.time);

    } catch(Exception exception) {
      exception.printStackTrace();
    }
  }

  /**
   * This is a simple method to create a notification
   * */
  protected void createNotification(Bandwidth bandwith) {

    final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
    builder.setContentTitle("Bandwidth")
        .setAutoCancel(true)
        .setColor(getResources().getColor(R.color.colorAccent))
        .setContentText(bandwith.speed+" Mo/s")
        .setSmallIcon(R.drawable.notification_icon);

    PendingIntent pendingIntent = PendingIntent.getActivity(this,
        NOTIFICATION_ID,
        new Intent(this, NotificationActivity.class),
        PendingIntent.FLAG_UPDATE_CURRENT);
    builder.setContentIntent(pendingIntent);

    final NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
    manager.notify(NOTIFICATION_ID, builder.build());

  }
}
