package com.demo.internetspeedtest.constants;

/**
 * Constants interface
 */

public interface Constants
{
  public static final String SPEED_TEST_BASE_URL_1_MO = "http://2.testdebit.info/fichiers/1Mo.dat";
  public static final long DELAY = 60*1000;
}
